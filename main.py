# import the necessary packages
from __future__ import print_function
from contextlib import ExitStack
import os
import cv2

from video_providers import FileProvider, StreamProvider
from video_outputs import FileOutput, ScreenOutput
from utils import CSVWriter, reduce_csv
from image_processors import FaceDetector, ObjectTracker, FaceIdentifier


# config
dir_path = os.path.dirname(os.path.realpath(__file__))
detection_method = 'hog'
output_file = os.path.join(dir_path, 'test.avi')
csv_fine = os.path.join(dir_path, 'out.csv')        # one entry per frame
csv_reduced = os.path.join(dir_path, 'out_red.csv') # one entry per second


#input_provider = StreamProvider()
input_provider = FileProvider('/home/nico/cruzers/video1.mp4')
output_writers = [] #[ScreenOutput(), FileOutput(output_file)]
processors = [FaceDetector(detection_method=detection_method, draw=True),
              FaceIdentifier("encodingsTC.pickle"),
              ObjectTracker(draw=True)]
csv_writer = CSVWriter(csv_fine)


# enter context of all providers/writers
with ExitStack() as stack:
    input = stack.enter_context(input_provider)
    outputs = [stack.enter_context(op) for op in output_writers]
    csv = stack.enter_context(csv_writer)
    glob_properties = {}
    prev_progress = 0
    while True:
        frame = input.read()

        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        properties = {}

        for p in processors:
            p.process(frame, properties, glob_properties)

        #print(properties)
        #print(glob_properties)
        #print('Target found: %d' % glob_properties.get('target_found', False))
        #print(input.get_time())
        if ((input.get_progress() - prev_progress) > 0.01):
            print('Progress: %f\n' % input.get_progress()*100)
            prev_progress = input.get_progress()

        csv.write(input.get_time(), glob_properties.get('tracking', False))

        # write to outputs
        for op in outputs:
            op.write(frame)

        # key to exit
        #key = cv2.waitKey(1) & 0xFF
        #if key == ord("q"):
        #    break
        if input.get_progress() == 1:
            break

reduce_csv(csv_fine, csv_reduced)
