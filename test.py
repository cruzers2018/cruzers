import os
from utils import get_images


dir_path = os.path.dirname(os.path.realpath(__file__))
img_dir = os.path.join(dir_path, 'datasets')


attributes = ['','Profile','Young','Old','MissionImpossible','Night','Injured','TropicThunder']
photoNumber = 5
for attr in attributes :
    get_images(os.path.join(img_dir,'TomCruise'), n_max = photoNumber, topic = attr, q='Tom Cruise '+attr)
