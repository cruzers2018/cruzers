import os
import cv2


class FileOutput:

    def __init__(self, output_path):
        self.writer = None
        self.output = output_path
        if not os.path.isdir(os.path.dirname(output_path)):
            raise IOError('Output path % does not exist' % os.path.dirname(output_path))

    def write(self, frame):
        if self.writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"MJPG")
            self.writer = cv2.VideoWriter(self.output, fourcc, 20,
                                     (frame.shape[1], frame.shape[0]), True)
        self.writer.write(frame)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.writer:
            self.writer.release()


class ScreenOutput:

    def __init__(self):
        pass

    def write(self, frame):
        cv2.imshow("Frame", frame)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        cv2.destroyAllWindows()
