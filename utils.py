import requests
import os
import math


KEY1 = "e1864a6ed454437789fb66b55f3b34ec"
KEY2 = "8e256e36fa7e4fb8b7c43a202c5c9673"
URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"
N_GROUP = 150


def get_images(output_dir, n_max, topic, **kwargs):
    headers = {"Ocp-Apim-Subscription-Key": KEY1}
    def_args = {"imageType": "photo", "count": str(n_max)}
    search_term = kwargs['q']
    # output_dir = os.path.join(output_dir, search_term)
    output_dir = os.path.join(output_dir)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    params = {**def_args, **kwargs}
    print('Searching for %s' % search_term)
    img_urls = []
    for k in range(0, math.floor(n_max/N_GROUP)+1):
        if min(N_GROUP, n_max - k*N_GROUP) <= 0:
            break
        params['count'] = str(min(N_GROUP, n_max - k*N_GROUP))
        params['offset'] = str(k * N_GROUP)
        #print(params)
        response = requests.get(URL, headers=headers, params=params)
        response.raise_for_status()
        search_results = response.json()
        #print('Found %d images' % len(search_results['value']))
        img_urls += [(img["contentUrl"], img["encodingFormat"]) for img in search_results["value"][:n_max]]
    i = 1
    print('Downloading %d images...' % len(img_urls))
    for url in img_urls:
        r = requests.get(url[0], stream=True)
        r.raise_for_status()
        file_format = url[1]
        path = os.path.join(output_dir, str(topic) + str(i)+"."+file_format)
        i += 1
        with open(path, 'wb') as f:
            for chunk in r:
                f.write(chunk)


class CSVWriter:

    def __init__(self, path):
        if not os.path.isdir(os.path.dirname(path)):
            raise IOError("CSV directory not found")
        self.file = None
        self.path = path

    def __enter__(self):
        self.file = open(self.path, 'w')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

    def write(self, second, target_found):
        self.file.write("%f,%d\n" % (second, (1 if target_found else 0)))


def reduce_csv(pin, pout):
    if not os.path.isfile(pin):
        raise IOError("CSV input file %s does not exist" % pin)
    with open(pin, 'r') as fin, open(pout, 'w') as fout:
        current_sec = 1
        target_found = 0
        for line in fin:
            (t, found) = line.split(',')
            t = float(t)
            found = int(found)
            if current_sec < t:
                fout.write("%d,%d\n" % (current_sec, target_found))
                target_found = found
                current_sec = math.ceil(t)
            else:
                if found == 1:
                    target_found = 1

        if t > current_sec:
            fout.write("%d,%d\n" % (math.ceil(t), target_found))
