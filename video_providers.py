import cv2
import os
import time
from imutils.video import VideoStream


class FileProvider:

    def __init__(self, file_path):
        print("[INFO] initializing FileProvider...")
        if not os.path.exists(file_path):
            raise ValueError("Video file %s does not exist" % file_path)
        self.path = file_path
        self.vidcap = cv2.VideoCapture()
        self.frame_count = 0
        self.frame_rate = 0
        self.length = 0
        self.count = 0

    def read(self):
        success, image = self.vidcap.read()
        #if success:
        self.count += 1
        return image

    def get_time(self):
        #return float(self.count) / self.frame_count * self.length
        #return float(self.count)/self.vidcap.get(cv2.CAP_PROP_FPS)
        return self.vidcap.get(cv2.CAP_PROP_POS_MSEC)/1000

    def get_progress(self):
        return float(self.vidcap.get(cv2.CAP_PROP_POS_FRAMES))/self.vidcap.get(cv2.CAP_PROP_FRAME_COUNT)

    def __enter__(self):
        self.vidcap.open(self.path)
        self.frame_rate = self.vidcap.get(cv2.CAP_PROP_FPS)
        self.frame_count = self.vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
        self.length = float(self.frame_count)/self.frame_rate
        print('Framerate: %d' % self.frame_rate)
        print('Total frames: %d' % self.frame_count)
        print('Total length: %d' % self.length)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.vidcap.release()


class StreamProvider:

    def __init__(self, src=0):
        print("[INFO] initializing StreamProvider...")
        self.source = src
        self.vs = VideoStream(src=self.source)

    def read(self):
        return self.vs.read()

    def get_time(self):
        return 0.0

    def get_progress(self):
        return 0.0

    def __enter__(self):
        self.vs.start()
        time.sleep(2.0)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.vs.stop()




#def videoLoading(startFrame,endFrame) :
#    for i in range(startFrame,endFrame) :
#      #cv2.imwrite("frame%d.jpg" % count, image)     # save frame as JPEG file
#          success,image = vidcap.read()
#          cv2.imshow('image',image)
#          print('Read a new frame: ', success)
