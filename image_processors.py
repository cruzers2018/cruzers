import imutils
import face_recognition
import cv2
import dlib
import pickle
import statistics
import math


class FaceDetector:

    def __init__(self, detection_method='hog', draw=False):
        self.detection_method = detection_method
        self.draw = draw
        self.frontal_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        #self.frontal_cascade = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')
        #self.profile_cascade = cv2.CascadeClassifier('haarcascade_profileface.xml')
        self.profile_cascade = cv2.CascadeClassifier('haarcascade_profileface2.xml')
        self.counter = 0
        self.last_face_boxes = []

    def process(self, frame, properties, glob_properties):
        if glob_properties.get('tracking', False):
            return

        self.counter += 1
        if self.counter < 3:
            properties['face_boxes'] = self.last_face_boxes
            if self.draw:
                for bb in self.last_face_boxes:
                    cv2.rectangle(frame, (bb[0], bb[1]), (bb[2], bb[3]), (0, 255, 0), 2)
            return

        self.counter = 0
        resized = imutils.resize(frame, width=min(320, frame.shape[1]))
        #resized = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        resized = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        flipped = cv2.flip(resized, 1)
        r = frame.shape[1] / float(resized.shape[1])

        # boxes = face_recognition.face_locations(resized, model=self.detection_method)
        cascade_results = []
        cascade_results.append((self.frontal_cascade.detectMultiScale(resized, 1.3, 5), False))  # x, y, w, h
        cascade_results.append((self.profile_cascade.detectMultiScale(resized, 1.3, 5), False))
        cascade_results.append((self.profile_cascade.detectMultiScale(flipped, 1.3, 5), True))
        properties['face_boxes'] = []

        #for (top, right, bottom, left) in boxes:
        for (boxes, is_flipped) in cascade_results:
            for (x, y, w, h) in boxes:
                # rescale the face coordinates
                top = int(y*r)
                right = frame.shape[1] - int((x+w)*r) if is_flipped else int((x+w)*r)
                bottom = int((y+h)*r)
                left = frame.shape[1] - int(x*r) if is_flipped else int(x*r)

                # old
                #top = int(top * r)
                #right = int(right * r)
                #bottom = int(bottom * r)
                #left = int(left * r)

                properties['face_boxes'].append((left, top, right, bottom))

                # draw the predicted face name on the image
                if self.draw:
                    cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 2)

        self.last_face_boxes = properties['face_boxes']


class FaceIdentifier:

    def __init__(self, path):
        data = pickle.loads(open(path, "rb").read())
        self.means=[]
        for i in range(128):
            self.means.append(statistics.mean([e[i] for e in data["encodings"][:]]))
        self.previous_boxes = []

    def process(self, frame, properties, glob_properties):
        face_boxes = properties.get('face_boxes', [])

        if glob_properties.get('tracking', False) or len(face_boxes) == 0:
            #face_boxes = [properties.get('tracker_box')]
            if not glob_properties.get('tracking', False):
                glob_properties['target_found'] = False
                properties['target_box'] = None
            return

        sorted_boxes = sorted(face_boxes, key=lambda e: e[0])   # sort by x
        if len(sorted_boxes) == len(self.previous_boxes):
            dist = 0
            for i in range(0, len(sorted_boxes)):
                center_a = (float(sorted_boxes[i][2] - sorted_boxes[i][0])/2,
                            float(sorted_boxes[i][3] - sorted_boxes[i][1])/2)
                center_b = (float(self.previous_boxes[i][2] - self.previous_boxes[i][0]) / 2,
                            float(self.previous_boxes[i][3] - self.previous_boxes[i][1]) / 2)
                dist += (center_a[0] - center_b[0])**2 + (center_a[1] - center_b[1])**2
            #print('Dist: %f' % dist)
            if dist/(len(sorted_boxes)**2) < 900:       # can be tuned
                glob_properties['target_found'] = False
                properties['target_box'] = None
                #print('Dist small')
                return
        self.previous_boxes = sorted_boxes

        target_box = None
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # detect the (x, y)-coordinates of the bounding boxes corresponding
        # to each face in the input image, then compute the facial embeddings
        # for each face
        # boxes = [(386, 885, 739, 533)] encodings = face_recognition.face_encodings(rgb, boxes)

        target_found = False
        for face_box in face_boxes:
            # print('1',face_box)
            reordered_box = [(face_box[1],face_box[2],face_box[3],face_box[0])]
            # print('2',face_box)
            encodings = face_recognition.face_encodings(rgb, reordered_box)
            # matches = face_recognition.compare_faces(self.data["encodings"],encodings[0])
            #change it by new method
            # print(data["encodings"])
            # print(encodings)
            # if True in matches:
            #     #count=count+1
            #     target_found = True
            #     target_box = face_box
            #     break

            if sum(abs(self.means - encodings[0]))<4.5:
                #count=count+1
                target_found = True
                target_box = face_box
                break
            #matches = face_recognition.compare_faces(data["encodings"],encodings)
            # check if face is Tom's - face_box is tuple: (left, top, right, bottom)
            # set target_found

        glob_properties['target_found'] = target_found
        properties['target_box'] = target_box    # replace this by tuple of identified face_box if found


class ObjectTracker:

    # DEPRECATED
    # OPENCV_OBJECT_TRACKERS = {
    #     "csrt": cv2.TrackerCSRT_create,
    #     "kcf": cv2.TrackerKCF_create,
    #     "boosting": cv2.TrackerBoosting_create,
    #     "mil": cv2.TrackerMIL_create,
    #     "tld": cv2.TrackerTLD_create,
    #     "medianflow": cv2.TrackerMedianFlow_create,
    #     "mosse": cv2.TrackerMOSSE_create
    # }

    def __init__(self, draw=False, quality_threshold=8.75):
        self.initBB = None
        self.draw = draw
        #self.tracker_name = tracker
        self.tracker = dlib.correlation_tracker() #self.OPENCV_OBJECT_TRACKERS[tracker]()
        self.tracking = False
        self.quality_threshold = quality_threshold

    def process(self, frame, properties, glob_properties):
        properties['tracker_box'] = None
        #resized = imutils.resize(frame, width=500)
        #(H, W) = resized.shape[:2]
        if self.tracking:
            # grab the new bounding box coordinates of the object
            quality = self.tracker.update(frame)

            # check to see if the tracking was a success
            if quality >= self.quality_threshold:
                position = self.tracker.get_position()

                #(x, y, w, h) = [int(v) for v in box]
                (x, y, w, h) = [int(position.left()), int(position.top()), int(position.width()), int(position.height())]
                properties['tracker_box'] = (x, y+h, x+w, y)
                if self.draw:
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            else:
                self.tracking = False

        if properties.get('target_box', None) is not None:
            # select the bounding box of the object we want to track (make
            # sure you press ENTER or SPACE after selecting the ROI)
            initBB = properties['target_box']
            #print(initBB)
            #self.tracker.init(frame, self.initBB)
            self.tracker.start_track(frame, dlib.rectangle(initBB[0], initBB[1], initBB[2], initBB[3]))
            self.tracking = True
            #print(initBB)

        glob_properties['tracking'] = self.tracking


class PedestrianDetector:

    def __init__(self):
        self.hog = cv2.HOGDescriptor()
        self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    def process(self, draw, properties, glob_properties):
        pass
        """(rects, weights) = hog.detectMultiScale(frame, winStride=(4, 4),
                                                padding=(8, 8), scale=1.05)
        for (x, y, w, h) in rects:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)

        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        for (xA, yA, xB, yB) in pick:
            cv2.rectangle(frame, (xA, yA), (xB, yB), (0, 255, 0), 2)

        for (top, right, bottom, left) in boxes:
            # rescale the face coordinates
            top = int(top * r)
            right = int(right * r)
            bottom = int(bottom * r)
            left = int(left * r)

            # draw the predicted face name on the image
            cv2.rectangle(frame, (left, top), (right, bottom),
                          (0, 255, 0), 2)"""
